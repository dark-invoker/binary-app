FROM ubuntu:16.04

RUN apt-get -y update
RUN apt-get -y install apache2

RUN echo '<h1>Hello world, Binary Studio</h1>' > /var/www/html/index.html

CMD ["/usr/sbin/apache2ctl", "-DFOREGROUND"]

EXPOSE 80
